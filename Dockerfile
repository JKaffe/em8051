FROM rust:slim-bookworm
LABEL authors="Karmjit Mahil"
LABEL description="Build image for the emgb project."

RUN apt update && \
    apt install -y meson gcc
/* SPDX-License-Identifier: LGPL-3.0-or-later */

mod emgb;

use anyhow::{anyhow, Context, Result};
use clap::Command;
use std::io::Write;

static SHELL_PS1: &str = "emgb $ ";

fn print_startup_notice() {
    println!(
        "{program_name}  Copyright (C) 2024  {authors}
License LGPLv3+: GNU LGPL version 3 or later <https://gnu.org/licenses/lgpl.html>
This is free software, and you are welcome to redistribute it
There is NO WARRANTY, to the extent permitted by law.
Type `show l' for details.",
        program_name = clap::crate_name!(),
        authors = clap::crate_authors!()
    )
}

fn main() -> Result<()> {
    print_startup_notice();

    let mut is_exit_requested = false;

    while !is_exit_requested {
        print!("{}", SHELL_PS1);
        let _ = std::io::stdout().flush();

        let mut line = String::new();
        std::io::stdin()
            .read_line(&mut line)
            .with_context(|| "Failed to read command line.")?;

        let line = line.trim();
        if line.is_empty() {
            continue;
        }

        is_exit_requested = process(line).unwrap_or_else(|e| {
            eprintln!("Error: {}\n", e);
            false
        })
    }

    Ok(())
}
fn cli() -> Command {
    Command::new("repl")
        .multicall(true)
        .arg_required_else_help(true)
        .subcommand_required(true)
        .subcommand_help_heading(clap::crate_name!())
        .subcommand(
            Command::new("exit")
                .about("Exit the emulator.")
                .alias("quit")
                .alias("q"),
        )
        .subcommand(
            Command::new("show")
                .arg_required_else_help(true)
                .subcommand_required(true)
                .about("Show licensing information.")
                .subcommand(
                    Command::new("license")
                        .about("Show license information.")
                        .alias("l"),
                ),
        )
        .subcommand(Command::new("test").about("Test initial C interop."))
}

fn process(line: &str) -> Result<bool> {
    let args =
        shlex::split(line).ok_or_else(|| anyhow!("Failed extracting command line arguments"))?;

    let matches = cli()
        .subcommand_required(true)
        .arg_required_else_help(true)
        .allow_external_subcommands(true)
        .try_get_matches_from(args);

    let matches = match matches {
        Ok(x) => Ok(x),
        Err(e) => match e.kind() {
            clap::error::ErrorKind::DisplayHelp
            | clap::error::ErrorKind::DisplayHelpOnMissingArgumentOrSubcommand
            | clap::error::ErrorKind::DisplayVersion => {
                print!("{}", e);
                return Ok(false);
            }
            e => Err(anyhow!("{}", e.to_string())),
        },
    }?;

    match matches.subcommand() {
        Some(("exit", _)) => {
            return Ok(true);
        }

        Some(("show", args)) => match args.subcommand() {
            Some(("license", _)) => println!("{}", include_str!("../COPYING")),
            Some((_, _)) => unreachable!("Implementation is missing some arg handling."),
            None => unreachable!(),
        },

        Some(("test", _)) => unsafe {
            emgb::cTest();
        },

        Some((cmd, _)) => {
            return Err(anyhow!("Unknown \'{cmd}\' command"));
        }

        None => unreachable!(),
    }

    Ok(false)
}

# GameBoy Emulator (emgb)

Status: In-development / Incomplete.

## Running the emulator shell

### Native

```bash
cargo run --release
```

If you try to run the executable directly you will most likely hit this error:
`error while loading shared libraries: libemgb.so`.

The executable produced by the build is a shell which loads the `emgb`
library. To fix the issue, prepend the executable with `LD_LIBRARY_PATH`,
setting it to the directory containing the library.

To find the latest `libemgb.so` built:

```bash
find "$PWD" -name 'libemgb.so' -printf "%T+ %p\n" | sort -r | head -n 1 | cut -d' ' -f2
```

### Docker

```bash
docker run --rm --user "$(id -u)":"$(id -g)" -v "$PWD":/usr/src/app -w /usr/src/app -it emgb_builder cargo run --release
```

## How to build?

### Native

```bash
cargo build --release
```

#### To build just the `emgb` library without the shell:

```bash
cd clib
meson setup --buildtype release build && meson compile -C build
cd -
```

### Docker

```bash
docker build -t emgb_builder .
docker run --rm --user "$(id -u)":"$(id -g)" -v "$PWD":/usr/src/app -w /usr/src/app emgb_builder cargo build --verbose --release
```

## License

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.
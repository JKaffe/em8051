/* SPDX-License-Identifier: LGPL-3.0-or-later */

#pragma once

#define EXPORT __attribute__((visibility("default")))

EXPORT void cTest(void);
/* SPDX-License-Identifier: LGPL-3.0-or-later */

extern crate meson;
use std::env;
use std::path::PathBuf;

fn main() {
    let build_path = PathBuf::from(env::var("OUT_DIR").unwrap());
    let build_path = build_path.join("build");
    let build_path = build_path.to_str().unwrap();

    println!("cargo:rerun-if-changed=clib");
    println!("cargo:rustc-link-search=native={}", build_path);
    meson::build("clib", build_path);

    println!("cargo:rustc-env=LD_LIBRARY_PATH={}", build_path);
}
